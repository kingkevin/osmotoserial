motoserial
==========

the motoserial (motorola automatic serial) is a phone controller
it is used to power, flash, and communicate with Motorola C1XX phones
the purpose is to use it for osmocomBB

purpose
-------

I used the osmocom baseaband software on Motorola C118 phones to monitor GSM networks
to communicate with them, I used USB to UART converters (PL2302, CP2102, …) on the 2.5 mm TRS jack
to flash the phone with the osmocomBB firmware, remove and reinstall the battery, start osmocon (or leave it running), and press on the ON button to start the bootloader
but sometimes the software gets stuck, and for monitoring GSM networks, an automatic way to reflash the phones had to be found

technology
----------

instead of the ON button, a pin on the back of the phone can be used
but I noticed that pluging the charger had the same effect
and instead of the battery, a 5V power source can also be used
so the trick is to use 2 transistors: 1 to simulate battery re-plug, and 1 to simulate charger replug; and switch off and on both power sources
since they share the ground pin, p-MOSFET on the positive side had to be used
actually the order and switching timing is not too important
thus one transistor, switching both the battery and charger power sources, is enough
this way a more common n-channel MOSFET can be used, but the ground pin of serial should then also be switched

the battery can be replaced by a regular 5 V power source, the same as the one used for charging
if the voltage is above 4.2 V, the battery should not be charged
to prevent power coming back on the power source if the phone charges the battery, a diode should be use on the positive side on the battery power source
this would also decrease the voltage (due to the forward voltage drop of the diode) and bring it closer to the usual 3.7-4.2 V voltage level

the phone draws at 3.7 V on average:
- ~ 90 mA when idling
- ~ 120 mA when receiving
- ~ 220 mA when transmitting
when transmitting, peaks of 2 A can be reached
this can be lowered to 1 A when an 470 µF capacitor is used

hardware
========

files
-----

motoserial:
after several prototypes (see versions), this was the best solution
the PCB replaces the battery
I had too many missing or dead batteries (which will start charging only after 30 minutes, and only lasts 10 minutes)
large capacitors compensate for the large but brief power surges during transmition bursts
connect the PCB to the computer through a USB to UART converter with the following pins: 5V, TX, RX, DTR, GND
a good USB to UART converter would be the Silicon Labs CP2102 (or CP2104)
they handle baudrates slightly different from standard values (which the Mototola C1XX have) better than Prolific PL2303, but not as good than FTDI FT2232R
they are more expensive than Prolific PL2303 converters, but cheaper than FTDI FT2232R, and work quite fine
good and cheap converters are BAITE (betemcu.cn) BTE13-007, CNT-003, or B75937 (solder pin DTR)
the DTR (or RTS) signal is used control the power switch
CP2102 resets this signal when the serial connection is closed
FT2232R can only reset RTS once the option is set in EEPROM, but DTR goes and stays low the first time the serial port is opened
CP2102 also includes a 700 mA resetable switch on the 5V pin which will prevent draining too much power on the USB port
connect the UART port to a 2.5mm TRS stereo jack and plug it in the headphone to be able to flash and communicate with the phone
connect the CHG port to a jack barrel (1.1mm inner diameter and 3.0 mm outer diameter) and plug it in the charger port to able to start the phone
some phones also provides these pins on the back, but not all
only though hole components have been used because it could not be all SMD (large capacitors are very expensive, and header connections to weak)
this allows to have a single sided PCB which can also be produced in amateur labs

version
-------

0:
current working version

E:
now motoserial-phone version prototype built (also provides a UART port)
bugs:
- resistors are in the way of connections
- pull-up/limiting resistor misplaced. the nMOS gate oscillates around Vgs

D:
4 motoserial-phone prototypes built to work with the motoserial-cape
bugs:
- the central tab is a bit too long. it's hard to remove the battery

C:
second motoserial-cape prototype built
bugs:
- one ground copper connection in the plane cut because too thin
- 100nF dim the UART signals and should be removed

B:
first motoserial-cape (for BeagleBoard BeagleBone Black) prototype built
bugs:
- I used nMOS instead of pMOS, which won't work because the power connection on the phone shares ground with UART


A:
first motoserial-scrape prototype built using CircuitPro + ProtoMat S63
bugs:
- the hole for the USB and header pins are too wide. I hoped it would create some through hole plating thanks to capillarity, but it didn't
- wider vias annulus would be a good idea
- DTR it set (to low) when the serial port is opened, but is no reset (to high) when closed (FT4232H specific)
- RTS can be reset to high on close, but that need to be programmed in the FT4232H's EEPROM
- the 3.3 V on DTR can not drive the pMOS because it's switching 5.0 V, but V_GS < 1.1 V
- decoupling capacitors on the serial lines should improve the quality
- try if with less ferrite beads and decoupling capacitors PLL still works
- Silabs CP2108 would be cheaper and easier to use than FT4232H

FAQ
===

Q: What is the design goal?
A: Provide a cheap way for everyone to remotely control motorola C1XX phones.
By controlling the battery and charger power, it is possible to trigger the flashing procedure. This is done through the DTR signal of the USB to UART converter.
The PCB is one layer, and only used through hole components, making it easy to produce and solder.
The components are basic: diode, transistor, capacitor, resistor. This makes it cheap.

Q: Don't have electrolytic capacitors high ESR?
Ceramic capacitors don't come in such high values like 470µF.
Tantalum capacitors rarely come in through hole packages.
Either way, they would cost far too much.
Electrolytic capacitors can have high values, and are cheap, particularly in through hole packages.
The three capacitors are also in parallel, which lowers the global ESR.
You can still used low ESR electrolytic capacitors instead.
Since they are in free air, they should not heat up and dry out so fast.

Q: Can capacitors replace a battery?
I tested calling with them, and it works.
During transmission bursts I measured voltage drops less than 0.3V, which is acceptable.
It also depends on the power source and the USB to UART convert you use. They might have additional capacitors, or current limiters/fuses.

Q: Why so many cables and connections?
The inputs (from the USB to UART converter) and outputs (to the power/charging and serial/headphone sockets) are clearly labeled.
Some phones provide pins behind the battery to control power and UART. That would require using pogo pins, which would be more complicated and expensive.
External cables are simpler and it works on all phones.
Since the goal is to be able to restart the phone automatically in a monitoring setup, I only attach the cables once and let it run the rest of the time.

Q: Why not integrate the USB to UART converter on the board?
I would have to make a double sided board to put the surface mount converter on.
And the chip would cost more than a complete USB dongle from china.
This contradicts the design goal.
And you can pick any USB to UART converter, as long as it provides 5V and DTR.

Q: Why not control multiple phones?
The first designed included a USB device controlling 4 phones based on a FT4232 or CP2108.
But you still need the boards in the phone.
And USB to UART from china are far less expensive, even with an additional USB hub.
