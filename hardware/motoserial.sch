v 20140308 2
C 30000 1000 1 0 0 title.sym
{
T 31000 1700 5 10 1 1 0 0 1
date=$Date$
T 34900 1700 5 10 1 1 0 0 1
org=osmocom
T 34900 1400 5 10 1 1 0 0 1
authors=Kévin Redon
T 33500 2200 5 14 1 1 0 4 1
title=motoserial
T 31000 1400 5 10 1 1 0 0 1
version=$Version$
T 31000 1100 5 10 1 1 0 0 1
revision=$Revision$
T 34900 1100 5 10 1 1 0 0 1
licence=CERN OHL v.1.2
}
C 30200 3600 1 0 0 connector5-1.sym
{
T 32000 5100 5 10 0 0 0 0 1
device=CONNECTOR_5
T 30300 5300 5 10 1 1 0 0 1
refdes=J1
T 30200 3300 5 10 1 1 0 0 1
value=PC_UART
T 31800 4600 5 10 0 0 0 0 1
footprint=JUMPER5
T 30400 4200 5 10 0 0 0 0 1
note=connect to USB to UART converter, such as BAITE (betemcu.cn) BTE13-007, CNT-003, or B75937 (solder pin DTR). these include a ~700mA resetable fuse on 5V pin.
}
C 32000 3300 1 0 0 signal-ground.sym
{
T 32000 3700 5 10 0 0 0 0 1
device=net
T 32000 4100 5 10 0 0 0 0 1
footprint=none
}
C 31900 5200 1 0 0 5V-plus-1.sym
C 33500 4500 1 180 0 input-2.sym
{
T 33500 4300 5 10 0 0 180 0 1
net=RX:1
T 32900 3800 5 10 0 0 180 0 1
device=none
T 33000 4400 5 10 1 1 180 7 1
value=RX
}
C 32100 4000 1 0 0 output-2.sym
{
T 33000 4200 5 10 0 0 0 0 1
net=DTR:1
T 32300 4700 5 10 0 0 0 0 1
device=none
T 33000 4100 5 10 1 1 0 1 1
value=\_DTR\_
}
C 32100 4600 1 0 0 output-2.sym
{
T 33000 4800 5 10 0 0 0 0 1
net=TX:1
T 32300 5300 5 10 0 0 0 0 1
device=none
T 33000 4700 5 10 1 1 0 1 1
value=TX
}
C 39300 5500 1 180 0 output-2.sym
{
T 38400 5300 5 10 0 0 180 0 1
net=RX:1
T 39100 4800 5 10 0 0 180 0 1
device=none
T 38400 5400 5 10 1 1 180 1 1
value=RX
}
C 37900 5600 1 0 0 input-2.sym
{
T 37900 5800 5 10 0 0 0 0 1
net=TX:1
T 38500 6300 5 10 0 0 0 0 1
device=none
T 38400 5700 5 10 1 1 0 7 1
value=TX
}
C 41800 1900 1 0 1 connector_battery_motorola_SNN5749A.sym
{
T 41800 2900 5 10 0 0 0 6 1
device=motorola_SNN5749A
T 41500 2700 5 10 1 1 0 6 1
refdes=P3
T 41800 3300 5 10 0 0 0 6 1
footprint=connector_battery_motorola_SNN5749A.fp
T 40700 1800 5 10 1 1 180 6 1
value=PHONE_BAT
}
N 31900 5000 32100 5000 4
N 32100 5000 32100 5200 4
N 31900 4100 32100 4100 4
N 31900 3800 32100 3800 4
N 32100 3800 32100 3600 4
N 40100 2100 40000 2100 4
N 40000 2100 40000 1900 4
N 38100 2400 40100 2400 4
C 39400 1200 1 0 0 signal-ground.sym
{
T 39400 1600 5 10 0 0 0 0 1
device=net
T 39400 2000 5 10 0 0 0 0 1
footprint=none
}
C 38600 1200 1 0 0 signal-ground.sym
{
T 38600 1600 5 10 0 0 0 0 1
device=net
T 38600 2000 5 10 0 0 0 0 1
footprint=none
}
C 39300 4100 1 0 0 5V-plus-1.sym
C 39400 2900 1 0 0 signal-ground.sym
{
T 39400 3300 5 10 0 0 0 0 1
device=net
T 39400 3700 5 10 0 0 0 0 1
footprint=none
}
N 39300 5400 39500 5400 4
N 40500 5400 40700 5400 4
N 40700 5700 40500 5700 4
N 39500 5700 39300 5700 4
C 34300 4800 1 0 0 5V-plus-1.sym
C 36900 3900 1 0 0 signal-ground.sym
{
T 36900 4300 5 10 0 0 0 0 1
device=net
T 36900 4700 5 10 0 0 0 0 1
footprint=none
}
C 36100 2800 1 0 0 signal-ground.sym
{
T 36100 3200 5 10 0 0 0 0 1
device=net
T 36100 3600 5 10 0 0 0 0 1
footprint=none
}
C 33900 5200 1 270 0 input-2.sym
{
T 34100 5200 5 10 0 0 270 0 1
net=DTR:1
T 34600 4600 5 10 0 0 270 0 1
device=none
T 33900 4800 5 10 1 1 180 7 1
value=\_DTR\_
}
N 34500 4700 35000 4700 4
N 37000 5500 37000 5300 4
N 37000 4400 37000 4200 4
N 36200 3300 36200 3100 4
N 35200 3600 35600 3600 4
N 34000 3800 34000 3600 4
N 34000 3600 34200 3600 4
B 30000 2500 3500 4000 3 0 1 0 -1 -1 0 -1 -1 -1 -1 -1
B 33500 2500 4000 4000 3 0 1 0 -1 -1 0 -1 -1 -1 -1 -1
B 37500 1000 4500 5500 3 0 1 0 -1 -1 0 -1 -1 -1 -1 -1
T 37600 6100 9 20 1 0 0 0 1
PHONE
T 33600 6100 9 20 1 0 0 0 1
POWER
T 30100 6100 9 20 1 0 0 0 1
PC
N 38100 2800 38100 2400 4
C 39300 4100 1 270 0 capacitor_470uF-electrolytic_kemet_ESK477M6R3AE3AA.sym
{
T 40000 4100 5 10 0 0 270 0 1
device=capacitor polarized
T 39100 3800 5 10 1 1 0 0 1
refdes=C1
T 38900 3300 5 10 1 1 0 0 1
value=470µF
T 41800 4100 5 10 0 0 270 0 1
footprint=capacitor_470uF-electrolytic_kemet_ESK-E3_N.fp
T 39300 4100 5 10 0 0 0 0 1
note=energy storage replacing the battery to cope with high power used during transmission burst
}
C 38500 2400 1 270 0 capacitor_470uF-electrolytic_kemet_ESK477M6R3AE3AA.sym
{
T 39200 2400 5 10 0 0 270 0 1
device=capacitor polarized
T 38300 2200 5 10 1 1 0 0 1
refdes=C2
T 38100 1600 5 10 1 1 0 0 1
value=470µF
T 41000 2400 5 10 0 0 270 0 1
footprint=capacitor_470uF-electrolytic_kemet_ESK-E3_N.fp
T 38500 2400 5 10 0 0 0 0 1
note=energy storage replacing the battery to cope with high power used during transmission burst
}
C 39300 2400 1 270 0 capacitor_470uF-electrolytic_kemet_ESK477M6R3AE3AA.sym
{
T 40000 2400 5 10 0 0 270 0 1
device=capacitor polarized
T 39100 2200 5 10 1 1 0 0 1
refdes=C3
T 38900 1600 5 10 1 1 0 0 1
value=470µF
T 41800 2400 5 10 0 0 270 0 1
footprint=capacitor_470uF-electrolytic_kemet_ESK-E3_N.fp
T 39300 2400 5 10 0 0 0 0 1
note=energy storage replacing the battery to cope with high power used during transmission burst
}
C 40600 4900 1 0 0 connector_trs_cui_SP-2501.sym
{
T 41300 5900 5 10 1 1 0 0 1
refdes=P1
T 41100 5600 5 10 0 1 0 0 1
device=TRS
T 40600 4700 5 10 1 1 0 0 1
value=PHONE_UART
T 40700 7100 5 10 0 0 0 0 1
footprint=JUMPER3
}
N 31900 4700 32100 4700 4
N 31900 4400 32100 4400 4
C 41600 3900 1 0 1 connector_barrel_cui_PP-019.sym
{
T 40400 4300 5 10 1 1 0 6 1
refdes=P2
T 41500 4600 5 10 0 0 0 6 1
device=jack
T 41300 3700 5 10 1 1 0 6 1
value=PHONE_CHG
T 41500 6000 5 10 0 0 0 6 1
footprint=JUMPER2
}
C 37200 5800 1 180 0 ground.sym
{
T 36900 5800 5 10 0 0 180 0 1
net=PGND:1
}
C 41500 3600 1 0 0 ground.sym
{
T 41800 3600 5 10 0 0 0 0 1
net=PGND:1
}
C 40000 4600 1 0 0 ground.sym
{
T 40300 4600 5 10 0 0 0 0 1
net=PGND:1
}
C 39800 1600 1 0 0 ground.sym
{
T 40100 1600 5 10 0 0 0 0 1
net=PGND:1
}
N 38100 4100 40600 4100 4
N 41500 4100 41700 4100 4
N 41700 4100 41700 3900 4
N 40700 5100 40200 5100 4
N 40200 5100 40200 4900 4
C 34500 4100 1 0 0 resistor_10kO-axial_yageo_CFR-25JB-52-10K.sym
{
T 34500 4600 5 10 0 0 0 0 1
device=resistor
T 34600 4400 5 10 1 1 0 0 1
refdes=R2
T 35100 4400 5 10 1 1 0 0 1
value=10kΩ
T 34500 6000 5 10 0 0 0 0 1
footprint=ACY400
T 35100 4200 5 10 0 0 0 0 1
note=pull-up resistor
}
C 34200 3500 1 0 0 resistor_1kO-axial_yageo_CFR-25JB-52-1K.sym
{
T 34200 4000 5 10 0 0 0 0 1
device=resistor
T 34200 3800 5 10 1 1 0 0 1
refdes=R3
T 34800 3800 5 10 1 1 0 0 1
value=1kΩ
T 34200 5400 5 10 0 0 0 0 1
footprint=ACY400
T 34200 3500 5 10 0 0 0 0 1
note=anti-ringing
}
C 35000 4600 1 0 0 resistor_1kO-axial_yageo_CFR-25JB-52-1K.sym
{
T 35000 5100 5 10 0 0 0 0 1
device=resistor
T 35000 4900 5 10 1 1 0 0 1
refdes=R1
T 35600 4900 5 10 1 1 0 0 1
value=1kΩ
T 35000 6500 5 10 0 0 0 0 1
footprint=ACY400
T 35000 4600 5 10 0 0 0 0 1
note=inverter
}
N 35500 4200 35500 3600 4
N 34500 4200 34500 4800 4
N 36000 4700 36400 4700 4
N 36200 4700 36200 4200 4
C 35600 3100 1 0 0 transistor_nmos_fairchild_2N7000TA.sym
{
T 35600 4100 5 10 1 1 0 0 1
refdes=Q2
T 35600 4600 5 10 0 0 0 0 1
device=MOSFET
T 35600 3200 5 10 1 1 0 0 1
value=nMOS
T 35600 6200 5 10 0 0 0 0 1
footprint=TO92
T 35600 3100 5 10 0 0 0 0 1
note=inverter
}
C 36400 4200 1 0 0 transistor_nmos_alpha-omega_AOI518.sym
{
T 36300 5100 5 10 1 1 0 0 1
refdes=Q1
T 36400 5700 5 10 0 0 0 0 1
device=MOSFET
T 36400 4300 5 10 1 1 0 0 1
value=nMOS
T 36400 7100 5 10 0 0 0 0 1
footprint=TO251
T 36200 6000 5 10 0 0 0 0 1
note=power switch
}
C 37900 3700 1 270 0 diode_rectifier_diodes_1N4007.sym
{
T 38600 3700 5 10 0 0 270 0 1
device=rectifier
T 37700 3500 5 10 1 1 0 0 1
refdes=D1
T 38300 3500 5 10 1 1 0 0 1
value=1A
T 40400 3700 5 10 0 0 270 0 1
footprint=ACY300P
T 38200 3300 5 10 0 0 0 0 1
note=drop 5V source to battery level ~4.2V and prevent current flowback on charging attempt
}
N 38100 4100 38100 3700 4
C 39500 5600 1 0 0 resistor_1kO-axial_yageo_CFR-25JB-52-1K.sym
{
T 39500 6100 5 10 0 0 0 0 1
device=resistor
T 39400 5800 5 10 1 1 0 0 1
refdes=R4
T 40300 5800 5 10 1 1 0 0 1
value=1kΩ
T 39500 7500 5 10 0 0 0 0 1
footprint=ACY400
T 39500 5600 5 10 0 0 0 0 1
note=I/O protection
}
C 39500 5300 1 0 0 resistor_1kO-axial_yageo_CFR-25JB-52-1K.sym
{
T 39500 5800 5 10 0 0 0 0 1
device=resistor
T 39400 5500 5 10 1 1 0 0 1
refdes=R5
T 40300 5500 5 10 1 1 0 0 1
value=1kΩ
T 39500 7200 5 10 0 0 0 0 1
footprint=ACY400
T 39500 5300 5 10 0 0 0 0 1
note=I/O protection
}
