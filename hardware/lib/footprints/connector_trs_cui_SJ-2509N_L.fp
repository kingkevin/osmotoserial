# footprint for a 2.5mm jacl TRS
# manufacturer: CUI
# part number: SJ-2509N
# datasheet: http://www.cui.com/product/resource/sj-2509n.pdf
# class: least
Element["" "2.5mm TRS" "" "CUI SJ-2509N" 0 0 0 0 0 100 ""]
(
ElementLine[-5.0mm -6.5mm 3.4mm -6.5mm 0.2mm]
ElementLine[3.4mm -6.5mm 3.4mm 3.5mm 0.2mm]
ElementLine[3.4mm 3.5mm -5.0mm 3.5mm 0.2mm]
ElementLine[-5.0mm 3.5mm -5.0mm -6.5mm 0.2mm]
ElementLine[-2.5mm 3.5mm 2.5mm 3.5mm 0.2mm]
ElementLine[2.5mm 3.5mm 2.5mm 6.5mm 0.2mm]
ElementLine[2.5mm 6.5mm -2.5mm 6.5mm 0.2mm]
ElementLine[-2.5mm 6.5mm -2.5mm 3.5mm 0.2mm]
Pin[-3.0mm -0.5mm 1.6mm 0.0mm 1.6mm 1.6mm "" "" "hole"]
Pin[2.0mm -0.5mm 1.6mm 0.0mm 1.6mm 1.6mm "" "" "hole"]
Pin[0.0mm 0.0mm 1.4mm 0.4mm 1.5mm 1.1mm "" "1" ""]
Pin[2.5mm -4.0mm 1.4mm 0.4mm 1.5mm 1.1mm "" "2" ""]
Pin[-4.1mm -6.0mm 1.4mm 0.4mm 1.5mm 1.1mm "" "3" ""]
)
