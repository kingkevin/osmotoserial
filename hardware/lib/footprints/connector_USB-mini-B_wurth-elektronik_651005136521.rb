#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.1
CLEARANCE = 0.4

["least","nominal","maximum"].each do |cla|
  base = File.basename(__FILE__,".rb")
  name = "#{base}_#{cla[0,1].upcase}.fp"
  annulus = case cla
  when "least"
    0.30
  when "nominal"
    0.35
  when "maximum"
    0.50
  end
  File.open(name,"w") do |fp|
    # put some information
    fp.puts "# footprint for a USB mini-B socket"
    fp.puts "# manufacturer: Wurth Elektronik"
    fp.puts "# part number: 651005136521"
    fp.puts "# datasheet: http://katalog.we-online.de/em/datasheet/651005136521.pdf"
    fp.puts "# class: #{cla}"

    # define element
    # center is pin 3
    fp.puts element("Element",["","USB mini-B","","Wurth Elektronik 651005136521",:"0",:"0",:"0",:"0",:"0",:"100",""])
    fp.puts "("

    # outline
    bottom = -0.9+1.2/2
    top = bottom-3.15-4.65-1.2/2
    left = -7.7/2
    right = 7.7/2
    fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
    fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

    # mount/shield holes
    fp.puts element("Pin",[-(8.2-0.9)/2,-4.65-0.9,1.6+annulus,CLEARANCE,1.6+annulus+SOLDERMASK,1.6,"SHIELD","6",""])
    fp.puts element("Pin",[(8.2-0.9)/2,-4.65-0.9,1.6+annulus,CLEARANCE,1.6+annulus+SOLDERMASK,1.6,"SHIELD","6",""])
    fp.puts element("Pin",[-(8.2-0.9)/2,-0.9,1.6+annulus,CLEARANCE,1.6+annulus+SOLDERMASK,1.6,"SHIELD","6",""])
    fp.puts element("Pin",[(8.2-0.9)/2,-0.9,1.6+annulus,CLEARANCE,1.6+annulus+SOLDERMASK,1.6,"SHIELD","6",""])

    # pin holes
    fp.puts element("Pin",[1.6,0,0.6+annulus,CLEARANCE,0.6+annulus+SOLDERMASK,0.6,"VBUS","1",""])
    fp.puts element("Pin",[0.8,-1.2,0.6+annulus,CLEARANCE,0.6+annulus+SOLDERMASK,0.6,"D-","2",""])
    fp.puts element("Pin",[0,0,0.6+annulus,CLEARANCE,0.6+annulus+SOLDERMASK,0.6,"D+","3",""])
    fp.puts element("Pin",[-0.8,-1.2,0.6+annulus,CLEARANCE,0.6+annulus+SOLDERMASK,0.6,"ID","4",""])
    fp.puts element("Pin",[-1.6,0,0.6+annulus,CLEARANCE,0.6+annulus+SOLDERMASK,0.6,"GND","5",""])

    # end of element
    fp.puts ")"
  end
end
