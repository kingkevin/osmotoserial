#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "in"
SILKSCREEN = 0.01
SOLDERMASK = 0.075
CLEARANCE = 0.015
ANNULUS = 0.015

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint for a BeagleBone (Black) cape"
  fp.puts "# manufacturer: BeagleBoard.org"
  fp.puts "# part number: BeagleBone Black Cape"
  fp.puts "# datasheet: https://github.com/CircuitCo/BeagleBone-Black/blob/master/BBB_SRM.pdf?raw=true"

  # define element
  # center is center of device
  fp.puts element("Element",["","BeagleBone Black","","Cape",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  # left bottom corner is center
  x1 = 3.4
  x2 = x1
  y1 = -2.15+0.5
  y2 = -0.5
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x1 = 3.4-0.5
  x2 = 0.25
  y1 = 0
  y2 = y1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x1 = 0
  x2 = x1
  y1 = -0.25
  y2 = -0.825
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 = 0.8
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 = -1.525
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 -= 0.575
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 = -2.15
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 = 3.4-0.5
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])

  # ElementArc [X Y Width Height StartAngle DeltaAngle Thickness]
  fp.puts element("ElementArc",[3.4-0.5,-2.15+0.5,0.5,0.5,:"180",:"90",SILKSCREEN])
  fp.puts element("ElementArc",[3.4-0.5,-0.5,0.5,0.5,:"90",:"90",SILKSCREEN])
  fp.puts element("ElementArc",[0.25,-0.25,0.25,0.25,:"0",:"90",SILKSCREEN])


  # holes
  # Pin [rX rY Thickness Clearance Mask Drill "Name" "Number" SFlags]
  fp.puts element("Pin",[0.575,-0.125,0.125,0,0.125,0.125,"","","hole"])
  fp.puts element("Pin",[0.575,-2.025,0.125,0,0.125,0.125,"","","hole"])
  fp.puts element("Pin",[3.175,-0.25,0.125,0,0.125,0.125,"","","hole"])
  fp.puts element("Pin",[3.175,-1.9,0.125,0,0.125,0.125,"","","hole"])

  # pins
  HOLE = 0.04
  # for the non-Black version
#  5.times do |i|
#    fp.puts element("Pin",[0.275+i*0.1,-0.65,HOLE+2*ANNULUS,CLEARANCE,HOLE+3*ANNULUS,HOLE,"","P6_#{i*2+1}",""])
#    fp.puts element("Pin",[0.275+i*0.1,-0.65-0.1,HOLE+2*ANNULUS,CLEARANCE,HOLE+3*ANNULUS,HOLE,"","P6_#{i*2+2}",""])
#  end
  23.times do |i|
    fp.puts element("Pin",[0.775+i*0.1,-0.175,HOLE+2*ANNULUS,CLEARANCE,HOLE+3*ANNULUS,HOLE,"","P9_#{i*2+2}",""])
    fp.puts element("Pin",[0.775+i*0.1,-0.175+0.1,HOLE+2*ANNULUS,CLEARANCE,HOLE+3*ANNULUS,HOLE,"","P9_#{i*2+1}",""])
    fp.puts element("Pin",[0.775+i*0.1,-1.525-0.450,HOLE+2*ANNULUS,CLEARANCE,HOLE+3*ANNULUS,HOLE,"","P8_#{i*2+1}",""])
    fp.puts element("Pin",[0.775+i*0.1,-1.525-0.450-0.1,HOLE+2*ANNULUS,CLEARANCE,HOLE+3*ANNULUS,HOLE,"","P8_#{i*2+2}",""])
  end
    

  # end of element
  fp.puts ")"
end

