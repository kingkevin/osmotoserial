#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.075
CLEARANCE = 0.4

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint for a SMT crystal HC-49S"
  fp.puts "# manufacturer: TXC CORPORATION"
  fp.puts "# part number: 9C-12.000MAAJ-T"
  fp.puts "# datasheet: http://www.txccrystal.com/images/pdf/9c.pdf"

  # define element
  # center is center of device
  fp.puts element("Element",["","HC-49S","","TXC 9C-12.000MAAJ-T",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  bottom = 4.8/2
  top = -4.8/2
  left = -12.7/2
  right = 12.7/2
  fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
  fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

  # pads
  fp.puts element("Pad",[-4.0/2-2.0/2,0,-4.0/2-5.5+2.0/2,0,2.0,CLEARANCE,2.0+2*SOLDERMASK,"","1","square"])
  fp.puts element("Pad",[4.0/2+2.0/2,0,4.0/2+5.5-2.0/2,0,2.0,CLEARANCE,2.0+2*SOLDERMASK,"","2","square"])

  # end of element
  fp.puts ")"
end

