# encoding: utf-8
# written for ruby 2.1.0
# helper functions to generate gEDA pcb files (on stdout)

# simple method to clean the dimension
def dim(dimension)
  return dimension.round(3).to_s+UNIT
end

# print parameters within an element
def element(name,params)
  to_return = ""
  to_return += name
  to_return += '['
  params.collect! do |param|
    case param
    when Symbol
      param.to_s
    when String
      '"'+param+'"'
    when Fixnum, Float
      dim(param)
    else 
      raise "unknowm paramter type: #{param.class}"
    end
  end
  to_return += params*' '
  to_return += ']'
  return to_return
end
