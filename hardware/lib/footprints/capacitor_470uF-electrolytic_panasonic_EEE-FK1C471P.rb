#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.075
CLEARANCE = 0.4

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint for an electrolytic capacitor"
  fp.puts "# manufacturer: Panasonic"
  fp.puts "# part number: EEEFK1C471P"
  fp.puts "# datasheet: https://industrial.panasonic.com/www-data/pdf/ABA0000/ABA0000CE120.pdf"
  fp.puts "# footprint: http://www.panasonic.net/id/ctlg/data/pdf/ABA0000/ABA0000PE269.pdf"

  # define element
  # center is center of device
  fp.puts element("Element",["","Panasonic EEEFK1C471P","","size code F",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  bottom = 8.3/2
  top = -1*bottom
  right = 8.3/2
  left = -1*right
  fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
  fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

  # add corner marker
  fp.puts element("ElementLine",[left,bottom-2,left+2,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[right,bottom-2,right-2,bottom,SILKSCREEN])

  # pads
  width = 2.0
  fp.puts element("Pad",[0,-3.1/2-width/2,0,-3.1/2-4.0+width/2,width,CLEARANCE,width+2*SOLDERMASK,"","-","square"])
  fp.puts element("Pad",[0,3.1/2+width/2,0,3.1/2+4.0-width/2,width,CLEARANCE,width+2*SOLDERMASK,"","+","square"])

  # end of element
  fp.puts ")"
end

