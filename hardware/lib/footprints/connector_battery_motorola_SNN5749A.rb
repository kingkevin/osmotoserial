#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.075
CLEARANCE = 0.4

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint as motorola SNN5749A battery"
  fp.puts "# manufacturer: Motorola"
  fp.puts "# part number: SNN5749A"

  # define element
  # center is center of device
  fp.puts element("Element",["","Motorola SNN5749A","","battery",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  x1 = -36.0/2
  x2 = x1+3
  y1 = 56/2
  y2 = y1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 += 1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 += 3
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 -= 1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 += 36.0-4*3.0
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 += 1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 += 3
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 -= 1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 += 3
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 -= 56
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 -= 13.3
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 -= 0.5
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 -= 6.85
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 += 0.5
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 = -36.0/2
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 += 56
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])

  # pads
  width = 1.6
  fp.puts element("Pad",[-36.0/2+2.3+width/2,-56.0/2+width/2,-36.0/2+2.3+width/2,-56.0/2+4.5-width/2,width,CLEARANCE,width+2*SOLDERMASK,"","-","square"])
  fp.puts element("Pad",[-36.0/2+4.8+width/2,-56.0/2+width/2,-36.0/2+4.8+width/2,-56.0/2+4.5-width/2,width,CLEARANCE,width+2*SOLDERMASK,"","","square"])
  fp.puts element("Pad",[-36.0/2+7.3+width/2,-56.0/2+width/2,-36.0/2+7.3+width/2,-56.0/2+4.5-width/2,width,CLEARANCE,width+2*SOLDERMASK,"","+","square"])

  # end of element
  fp.puts ")"
end

