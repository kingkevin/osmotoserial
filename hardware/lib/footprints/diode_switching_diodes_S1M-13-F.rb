#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.075
CLEARANCE = 0.4

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint for a switching diode"
  fp.puts "# manufacturer: Diodes"
  fp.puts "# part number: S1M-13-F"
  fp.puts "# datasheet: http://www.diodes.com/datasheets/ds16003.pdf"

  # define element
  # center is center of device
  fp.puts element("Element",["","SMA","","diodes S1M-13-F",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  bottom = (2.29+2.92)/2/2
  top = -1*bottom
  right = (4.0+4.6)/2/2
  left = -1*right
  fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
  fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

  # add cathode band
  fp.puts element("ElementLine",[left+1,bottom,left+1,top,SILKSCREEN])

  # pads
  fp.puts element("Pad",[-4.0/2-2.5/2+1.7/2,0,-4.0/2+2.5/2-1,0,1.7,CLEARANCE,1.7+2*SOLDERMASK,"","C","square"])
  fp.puts element("Pad",[4.0/2-2.5/2+1.7/2,0,4.0/2+2.5/2-1,0,1.7,CLEARANCE,1.7+2*SOLDERMASK,"","A","square"])

  # end of element
  fp.puts ")"
end

