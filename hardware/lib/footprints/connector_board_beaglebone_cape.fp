# footprint for a BeagleBone (Black) cape
# manufacturer: BeagleBoard.org
# part number: BeagleBone Black Cape
# datasheet: https://github.com/CircuitCo/BeagleBone-Black/blob/master/BBB_SRM.pdf?raw=true
Element["" "BeagleBone Black" "" "Cape" 0 0 0 0 0 100 ""]
(
ElementLine[3.4in -1.65in 3.4in -0.5in 0.01in]
ElementLine[2.9in 0.0in 0.25in 0.0in 0.01in]
ElementLine[0.0in -0.25in 0.0in -0.825in 0.01in]
ElementLine[0.0in -0.825in 0.8in -0.825in 0.01in]
ElementLine[0.8in -0.825in 0.8in -1.525in 0.01in]
ElementLine[0.8in -1.525in 0.225in -1.525in 0.01in]
ElementLine[0.225in -1.525in 0.225in -2.15in 0.01in]
ElementLine[0.225in -2.15in 2.9in -2.15in 0.01in]
ElementArc[2.9in -1.65in 0.5in 0.5in 180 90 0.01in]
ElementArc[2.9in -0.5in 0.5in 0.5in 90 90 0.01in]
ElementArc[0.25in -0.25in 0.25in 0.25in 0 90 0.01in]
Pin[0.575in -0.125in 0.125in 0.0in 0.125in 0.125in "" "" "hole"]
Pin[0.575in -2.025in 0.125in 0.0in 0.125in 0.125in "" "" "hole"]
Pin[3.175in -0.25in 0.125in 0.0in 0.125in 0.125in "" "" "hole"]
Pin[3.175in -1.9in 0.125in 0.0in 0.125in 0.125in "" "" "hole"]
Pin[0.775in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_2" ""]
Pin[0.775in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_1" ""]
Pin[0.775in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_1" ""]
Pin[0.775in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_2" ""]
Pin[0.875in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_4" ""]
Pin[0.875in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_3" ""]
Pin[0.875in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_3" ""]
Pin[0.875in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_4" ""]
Pin[0.975in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_6" ""]
Pin[0.975in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_5" ""]
Pin[0.975in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_5" ""]
Pin[0.975in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_6" ""]
Pin[1.075in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_8" ""]
Pin[1.075in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_7" ""]
Pin[1.075in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_7" ""]
Pin[1.075in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_8" ""]
Pin[1.175in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_10" ""]
Pin[1.175in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_9" ""]
Pin[1.175in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_9" ""]
Pin[1.175in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_10" ""]
Pin[1.275in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_12" ""]
Pin[1.275in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_11" ""]
Pin[1.275in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_11" ""]
Pin[1.275in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_12" ""]
Pin[1.375in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_14" ""]
Pin[1.375in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_13" ""]
Pin[1.375in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_13" ""]
Pin[1.375in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_14" ""]
Pin[1.475in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_16" ""]
Pin[1.475in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_15" ""]
Pin[1.475in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_15" ""]
Pin[1.475in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_16" ""]
Pin[1.575in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_18" ""]
Pin[1.575in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_17" ""]
Pin[1.575in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_17" ""]
Pin[1.575in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_18" ""]
Pin[1.675in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_20" ""]
Pin[1.675in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_19" ""]
Pin[1.675in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_19" ""]
Pin[1.675in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_20" ""]
Pin[1.775in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_22" ""]
Pin[1.775in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_21" ""]
Pin[1.775in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_21" ""]
Pin[1.775in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_22" ""]
Pin[1.875in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_24" ""]
Pin[1.875in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_23" ""]
Pin[1.875in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_23" ""]
Pin[1.875in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_24" ""]
Pin[1.975in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_26" ""]
Pin[1.975in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_25" ""]
Pin[1.975in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_25" ""]
Pin[1.975in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_26" ""]
Pin[2.075in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_28" ""]
Pin[2.075in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_27" ""]
Pin[2.075in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_27" ""]
Pin[2.075in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_28" ""]
Pin[2.175in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_30" ""]
Pin[2.175in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_29" ""]
Pin[2.175in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_29" ""]
Pin[2.175in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_30" ""]
Pin[2.275in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_32" ""]
Pin[2.275in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_31" ""]
Pin[2.275in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_31" ""]
Pin[2.275in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_32" ""]
Pin[2.375in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_34" ""]
Pin[2.375in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_33" ""]
Pin[2.375in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_33" ""]
Pin[2.375in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_34" ""]
Pin[2.475in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_36" ""]
Pin[2.475in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_35" ""]
Pin[2.475in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_35" ""]
Pin[2.475in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_36" ""]
Pin[2.575in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_38" ""]
Pin[2.575in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_37" ""]
Pin[2.575in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_37" ""]
Pin[2.575in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_38" ""]
Pin[2.675in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_40" ""]
Pin[2.675in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_39" ""]
Pin[2.675in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_39" ""]
Pin[2.675in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_40" ""]
Pin[2.775in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_42" ""]
Pin[2.775in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_41" ""]
Pin[2.775in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_41" ""]
Pin[2.775in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_42" ""]
Pin[2.875in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_44" ""]
Pin[2.875in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_43" ""]
Pin[2.875in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_43" ""]
Pin[2.875in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_44" ""]
Pin[2.975in -0.175in 0.07in 0.015in 0.085in 0.04in "" "P9_46" ""]
Pin[2.975in -0.075in 0.07in 0.015in 0.085in 0.04in "" "P9_45" ""]
Pin[2.975in -1.975in 0.07in 0.015in 0.085in 0.04in "" "P8_45" ""]
Pin[2.975in -2.075in 0.07in 0.015in 0.085in 0.04in "" "P8_46" ""]
)
