# footprint for a USB mini-B socket
# manufacturer: Wurth Elektronik
# part number: 651005136521
# datasheet: http://katalog.we-online.de/em/datasheet/651005136521.pdf
# class: maximum
Element["" "USB mini-B" "" "Wurth Elektronik 651005136521" 0 0 0 0 0 100 ""]
(
ElementLine[-3.85mm -8.7mm 3.85mm -8.7mm 0.2mm]
ElementLine[3.85mm -8.7mm 3.85mm -0.3mm 0.2mm]
ElementLine[3.85mm -0.3mm -3.85mm -0.3mm 0.2mm]
ElementLine[-3.85mm -0.3mm -3.85mm -8.7mm 0.2mm]
Pin[-3.65mm -5.55mm 2.1mm 0.4mm 2.2mm 1.6mm "SHIELD" "6" ""]
Pin[3.65mm -5.55mm 2.1mm 0.4mm 2.2mm 1.6mm "SHIELD" "6" ""]
Pin[-3.65mm -0.9mm 2.1mm 0.4mm 2.2mm 1.6mm "SHIELD" "6" ""]
Pin[3.65mm -0.9mm 2.1mm 0.4mm 2.2mm 1.6mm "SHIELD" "6" ""]
Pin[1.6mm 0.0mm 1.1mm 0.4mm 1.2mm 0.6mm "VBUS" "1" ""]
Pin[0.8mm -1.2mm 1.1mm 0.4mm 1.2mm 0.6mm "D-" "2" ""]
Pin[0.0mm 0.0mm 1.1mm 0.4mm 1.2mm 0.6mm "D+" "3" ""]
Pin[-0.8mm -1.2mm 1.1mm 0.4mm 1.2mm 0.6mm "ID" "4" ""]
Pin[-1.6mm 0.0mm 1.1mm 0.4mm 1.2mm 0.6mm "GND" "5" ""]
)
