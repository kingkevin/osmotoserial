# footprint for an electrolytic capacitor
# manufacturer: Panasonic
# part number: EEEFK1C471P
# datasheet: https://industrial.panasonic.com/www-data/pdf/ABA0000/ABA0000CE120.pdf
# footprint: http://www.panasonic.net/id/ctlg/data/pdf/ABA0000/ABA0000PE269.pdf
Element["" "Panasonic EEEFK1C471P" "" "size code F" 0 0 0 0 0 100 ""]
(
ElementLine[-4.15mm -4.15mm 4.15mm -4.15mm 0.2mm]
ElementLine[4.15mm -4.15mm 4.15mm 4.15mm 0.2mm]
ElementLine[4.15mm 4.15mm -4.15mm 4.15mm 0.2mm]
ElementLine[-4.15mm 4.15mm -4.15mm -4.15mm 0.2mm]
ElementLine[-4.15mm 2.15mm -2.15mm 4.15mm 0.2mm]
ElementLine[4.15mm 2.15mm 2.15mm 4.15mm 0.2mm]
Pad[0.0mm -2.55mm 0.0mm -4.55mm 2.0mm 0.4mm 2.15mm "" "-" "square"]
Pad[0.0mm 2.55mm 0.0mm 4.55mm 2.0mm 0.4mm 2.15mm "" "+" "square"]
)
