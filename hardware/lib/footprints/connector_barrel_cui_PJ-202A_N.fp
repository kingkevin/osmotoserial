# footprint for a Jack Barrel
# manufacturer: CUI
# part number: PJ-202A
# datasheet: http://www.cui.com/product/resource/pj-202a.pdf
# class: nominal
Element["" "barrel" "" "CUI PJ-202A" 0 0 0 0 0 100 ""]
(
ElementLine[-3.7mm -4.5mm 10.7mm -4.5mm 0.2mm]
ElementLine[10.7mm -4.5mm 10.7mm 4.5mm 0.2mm]
ElementLine[10.7mm 4.5mm -3.7mm 4.5mm 0.2mm]
ElementLine[-3.7mm 4.5mm -3.7mm -4.5mm 0.2mm]
Pin[-3.0mm 0.0mm 3.85mm 0.4mm 3.95mm 3.5mm "" "1" ""]
Pin[3.0mm 0.0mm 3.35mm 0.4mm 3.45mm 3.0mm "" "2" ""]
Pin[0.0mm -4.7mm 3.35mm 0.4mm 3.45mm 3.0mm "" "3" ""]
)
