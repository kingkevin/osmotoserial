# footprint for a 1.1/3.0mm jack barrel
# manufacturer: CUI
# part number: PJ-043-SMT-TR
# datasheet: http://www.cui.com/product/resource/digikeypdf/pj-043-smt.pdf
Element["" "barrel" "" "CUI PJ-043-SMT-TR" 0 0 0 0 0 100 ""]
(
ElementLine[-8.5mm -2.3mm 1.9mm -2.3mm 0.2mm]
ElementLine[1.9mm -2.3mm 1.9mm -1.6mm 0.2mm]
ElementLine[1.9mm -1.6mm 3.5mm -1.6mm 0.2mm]
ElementLine[3.5mm -1.6mm 3.5mm 0.6mm 0.2mm]
ElementLine[3.5mm 0.6mm 1.9mm 0.6mm 0.2mm]
ElementLine[1.9mm 0.6mm 1.9mm 3.7mm 0.2mm]
ElementLine[1.9mm 3.7mm -7.0mm 3.7mm 0.2mm]
ElementLine[-7.0mm 3.7mm -7.0mm 2.3mm 0.2mm]
ElementLine[-7.0mm 2.3mm -8.5mm 2.3mm 0.2mm]
ElementLine[-8.5mm 2.3mm -8.5mm -2.3mm 0.2mm]
Pin[0.0mm 0.0mm 1.1mm 0.0mm 1.1mm 1.1mm "" "" "hole"]
Pin[-4.5mm 0.0mm 1.1mm 0.0mm 1.1mm 1.1mm "" "" "hole"]
Pad[-3.35mm -3.25mm -2.65mm -3.25mm 1.9mm 0.4mm 2.05mm "" "4" "square"]
Pad[-3.35mm 4.65mm -2.65mm 4.65mm 1.9mm 0.4mm 2.05mm "" "4" "square"]
Pad[2.45mm -2.65mm 2.55mm -2.65mm 2.1mm 0.4mm 2.25mm "" "3" "square"]
Pad[2.85mm 1.65mm 2.95mm 1.65mm 2.1mm 0.4mm 2.25mm "" "1" "square"]
Pad[0.15mm 3.85mm 0.25mm 3.85mm 2.1mm 0.4mm 2.25mm "" "2" "square"]
)
