#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.1
CLEARANCE = 0.4

["least","nominal","maximum"].each do |cla|
  base = File.basename(__FILE__,".rb")
  name = "#{base}_#{cla[0,1].upcase}.fp"
  annulus = case cla
  when "least"
    0.30
  when "nominal"
    0.35
  when "maximum"
    0.50
  end
  File.open(name,"w") do |fp|
    # put some information
    fp.puts "# footprint for a Jack Barrel"
    fp.puts "# manufacturer: CUI"
    fp.puts "# part number: PJ-202A"
    fp.puts "# datasheet: http://www.cui.com/product/resource/pj-202a.pdf"
    fp.puts "# class: #{cla}"

    # define element
    # center is pin 3
    fp.puts element("Element",["","barrel","","CUI PJ-202A",:"0",:"0",:"0",:"0",:"0",:"100",""])
    fp.puts "("

    # outline
    bottom = 9.0/2
    top = -9.0/2
    left = 10.7-14.4
    right = 10.7
    fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
    fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

    # pin holes
    fp.puts element("Pin",[-3.0,0,3.5+annulus,CLEARANCE,3.5+annulus+SOLDERMASK,3.5,"","1",""])
    fp.puts element("Pin",[3.0,0,3.0+annulus,CLEARANCE,3.0+annulus+SOLDERMASK,3.0,"","2",""])
    fp.puts element("Pin",[0,-4.7,3.0+annulus,CLEARANCE,3.0+annulus+SOLDERMASK,3.0,"","3",""])

    # end of element
    fp.puts ")"
  end
end
