#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.075
CLEARANCE = 0.4

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint for Silabs CP2108 QFN64"
  fp.puts "# manufacturer: Silicon Laboratories"
  fp.puts "# part number: CP2108-B01-GM"
  fp.puts "# datasheet: http://www.silabs.com/Support%20Documents/TechnicalDocs/CP2108.pdf"

  # define element
  # center is center of device
  fp.puts element("Element",["","CP2108 QFN64","","Silabs CP2108",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  top = -9.0/2
  bottom = 9.0/2
  left = -9.0/2
  right = 9.0/2
  fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
  fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

  # pin 1 hole
  fp.puts element("ElementArc",[-9.0/2+1.0,-9.0/2+1.0,0.25,0.25,:"0",:"360",SILKSCREEN])

  # pads
  16.times do |i|
    fp.puts element("Pad",[-8.90/2-0.85/2+0.30/2,-0.50*7.5+0.50*i,-8.90/2+0.85/2-0.30/2,-0.50*7.5+0.50*i,0.30,CLEARANCE,0.30+2*SOLDERMASK,"","#{i+1}",i==0 ? "square" : ""])
    fp.puts element("Pad",[8.90/2-0.85/2+0.30/2,-0.50*7.5+0.50*i,8.90/2+0.85/2-0.30/2,-0.50*7.5+0.50*i,0.30,CLEARANCE,0.30+2*SOLDERMASK,"","#{48-i}",""])
    fp.puts element("Pad",[-0.50*7.5+0.50*i,-8.90/2+0.85/2-0.30/2,-0.50*7.5+0.50*i,-8.90/2-0.85/2+0.30/2,0.30,CLEARANCE,0.30+2*SOLDERMASK,"","#{64-i}",""])
    fp.puts element("Pad",[-0.50*7.5+0.50*i,8.90/2-0.85/2+0.30/2,-0.50*7.5+0.50*i,8.90/2+0.85/2-0.30/2,0.30,CLEARANCE,0.30+2*SOLDERMASK,"","#{i+17}",""])
    fp.puts element("Pad",[0,0,0,0,4.25,CLEARANCE,0.30+2*SOLDERMASK,"","#{i+17}","square"])
  end

  # end of element
  fp.puts ")"
end

