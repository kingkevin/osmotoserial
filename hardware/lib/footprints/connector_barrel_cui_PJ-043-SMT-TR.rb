#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.075
CLEARANCE = 0.4

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint for a 1.1/3.0mm jack barrel"
  fp.puts "# manufacturer: CUI"
  fp.puts "# part number: PJ-043-SMT-TR"
  fp.puts "# datasheet: http://www.cui.com/product/resource/digikeypdf/pj-043-smt.pdf"

  # define element
  # center is center of device
  fp.puts element("Element",["","barrel","","CUI PJ-043-SMT-TR",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  x1 = -4.5-2.5-1.5
  x2 = x1+1.5+9.5-1.2/2
  y1 = -2.3
  y2 = y1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 = -1.6
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 = 10.5-2.5-4.5
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 = 0.6
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 = -4.5-2.5+9.9-1
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 = 6.0-2.3
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 = -4.5-2.5
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 = 4.6/2
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  x2 = x1-1.5
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])
  y2 = -4.6/2
  fp.puts element("ElementLine",[x1,y1,x1=x2,y1=y2,SILKSCREEN])

  # holes
  fp.puts element("Pin",[0,0,1.1,0,1.1,1.1,"","","hole"])
  fp.puts element("Pin",[-4.5,0,1.1,0,1.1,1.1,"","","hole"])

  # pads
  fp.puts element("Pad",[-4.5-2.5+4.0-2.6/2+1.9/2,-2.3-1.9/2,-4.5-2.5+4.0+2.6/2-1.9/2,-2.3-1.9/2,1.9,CLEARANCE,1.9+2*SOLDERMASK,"","4","square"])
  fp.puts element("Pad",[-4.5-2.5+4.0-2.6/2+1.9/2,6.0-2.3+1.9/2,-4.5-2.5+4.0+2.6/2-1.9/2,6.0-2.3+1.9/2,1.9,CLEARANCE,1.9+2*SOLDERMASK,"","4","square"])
  fp.puts element("Pad",[-4.5-2.5+9.5-2.2/2+2.1/2,-1.6-2.1/2,-4.5-2.5+9.5+2.2/2-2.1/2,-1.6-2.1/2,2.1,CLEARANCE,2.1+2*SOLDERMASK,"","3","square"])
  fp.puts element("Pad",[-4.5-2.5+9.9-2.2/2+2.1/2,0.6+2.1/2,-4.5-2.5+9.9+2.2/2-2.1/2,0.6+2.1/2,2.1,CLEARANCE,2.1+2*SOLDERMASK,"","1","square"])
  fp.puts element("Pad",[-4.5-2.5+7.2-2.2/2+2.1/2,2.8+2.1/2,-4.5-2.5+7.2+2.2/2-2.1/2,2.8+2.1/2,2.1,CLEARANCE,2.1+2*SOLDERMASK,"","2","square"])

  # end of element
  fp.puts ")"
end

