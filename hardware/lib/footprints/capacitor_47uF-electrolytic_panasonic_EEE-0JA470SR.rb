#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.075
CLEARANCE = 0.4

name = File.basename(__FILE__,".rb")+".fp"
File.open(name,"w") do |fp|
  # put some information
  fp.puts "# footprint for an electrolytic capacitor"
  fp.puts "# manufacturer: Panasonic"
  fp.puts "# part number: EEE-0JA470SR"
  fp.puts "# datasheet: https://industrial.panasonic.com/www-data/pdf/ABA0000/ABA0000CE120.pdf"
  fp.puts "# footprint: http://www.panasonic.net/id/ctlg/data/pdf/ABA0000/ABA0000PE269.pdf"

  # define element
  # center is center of device
  fp.puts element("Element",["","Panasonic EEE-0JA470SR","","size code C",:"0",:"0",:"0",:"0",:"0",:"100",""])
  fp.puts "("

  # outline
  bottom = 5.3/2
  top = -1*bottom
  right = 5.3/2
  left = -1*right
  fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
  fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

  # add corner marker
  fp.puts element("ElementLine",[left,bottom-1.5,left+1.5,bottom,SILKSCREEN])
  fp.puts element("ElementLine",[right,bottom-1.5,right-1.5,bottom,SILKSCREEN])

  # pads
  width = 1.6
  fp.puts element("Pad",[0,-1.5/2-width/2,0,-1.5/2-2.8+width/2,width,CLEARANCE,width+2*SOLDERMASK,"","-","square"])
  fp.puts element("Pad",[0,1.5/2+width/2,0,1.5/2+2.8-width/2,width,CLEARANCE,width+2*SOLDERMASK,"","+","square"])

  # end of element
  fp.puts ")"
end

