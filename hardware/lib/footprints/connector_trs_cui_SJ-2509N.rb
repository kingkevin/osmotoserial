#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.1
CLEARANCE = 0.4

["least","nominal","maximum"].each do |cla|
  base = File.basename(__FILE__,".rb")
  name = "#{base}_#{cla[0,1].upcase}.fp"
  annulus = case cla
  when "least"
    0.30
  when "nominal"
    0.35
  when "maximum"
    0.50
  end
  File.open(name,"w") do |fp|
    # put some information
    fp.puts "# footprint for a 2.5mm jacl TRS"
    fp.puts "# manufacturer: CUI"
    fp.puts "# part number: SJ-2509N"
    fp.puts "# datasheet: http://www.cui.com/product/resource/sj-2509n.pdf"
    fp.puts "# class: #{cla}"

    # define element
    # center is pin 3
    fp.puts element("Element",["","2.5mm TRS","","CUI SJ-2509N",:"0",:"0",:"0",:"0",:"0",:"100",""])
    fp.puts "("

    # outline
    bottom = 3.5
    top = 3.5-10.0
    left = -5.0
    right = 3.4
    fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
    fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])
    bottom = 3.5+3.0
    top = 3.5
    left = -5.0/2
    right = 5.0/2
    fp.puts element("ElementLine",[left,top,right,top,SILKSCREEN])
    fp.puts element("ElementLine",[right,top,right,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[right,bottom,left,bottom,SILKSCREEN])
    fp.puts element("ElementLine",[left,bottom,left,top,SILKSCREEN])

    # mounting holes
    fp.puts element("Pin",[-3.0,3.5-4.0,1.6,0,1.6,1.6,"","","hole"])
    fp.puts element("Pin",[2.0,3.5-4.0,1.6,0,1.6,1.6,"","","hole"])

    # pin holes
    fp.puts element("Pin",[0,0,1.1+annulus,CLEARANCE,1.1+annulus+SOLDERMASK,1.1,"","1",""])
    fp.puts element("Pin",[2.5,3.5-7.5,1.1+annulus,CLEARANCE,1.1+annulus+SOLDERMASK,1.1,"","2",""])
    fp.puts element("Pin",[-4.1,3.5-9.5,1.1+annulus,CLEARANCE,1.1+annulus+SOLDERMASK,1.1,"","3",""])

    # end of element
    fp.puts ")"
  end
end
