# footprint for a switching diode
# manufacturer: Diodes
# part number: S1M-13-F
# datasheet: http://www.diodes.com/datasheets/ds16003.pdf
Element["" "SMA" "" "diodes S1M-13-F" 0 0 0 0 0 100 ""]
(
ElementLine[-2.15mm -1.303mm 2.15mm -1.303mm 0.2mm]
ElementLine[2.15mm -1.303mm 2.15mm 1.303mm 0.2mm]
ElementLine[2.15mm 1.303mm -2.15mm 1.303mm 0.2mm]
ElementLine[-2.15mm 1.303mm -2.15mm -1.303mm 0.2mm]
ElementLine[-1.15mm 1.303mm -1.15mm -1.303mm 0.2mm]
Pad[-2.4mm 0.0mm -1.75mm 0.0mm 1.7mm 0.4mm 1.85mm "" "C" "square"]
Pad[1.6mm 0.0mm 2.25mm 0.0mm 1.7mm 0.4mm 1.85mm "" "A" "square"]
)
