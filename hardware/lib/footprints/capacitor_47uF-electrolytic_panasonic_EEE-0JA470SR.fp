# footprint for an electrolytic capacitor
# manufacturer: Panasonic
# part number: EEE-0JA470SR
# datasheet: https://industrial.panasonic.com/www-data/pdf/ABA0000/ABA0000CE120.pdf
# footprint: http://www.panasonic.net/id/ctlg/data/pdf/ABA0000/ABA0000PE269.pdf
Element["" "Panasonic EEE-0JA470SR" "" "size code C" 0 0 0 0 0 100 ""]
(
ElementLine[-2.65mm -2.65mm 2.65mm -2.65mm 0.2mm]
ElementLine[2.65mm -2.65mm 2.65mm 2.65mm 0.2mm]
ElementLine[2.65mm 2.65mm -2.65mm 2.65mm 0.2mm]
ElementLine[-2.65mm 2.65mm -2.65mm -2.65mm 0.2mm]
ElementLine[-2.65mm 1.15mm -1.15mm 2.65mm 0.2mm]
ElementLine[2.65mm 1.15mm 1.15mm 2.65mm 0.2mm]
Pad[0.0mm -1.55mm 0.0mm -2.75mm 1.6mm 0.4mm 1.75mm "" "-" "square"]
Pad[0.0mm 1.55mm 0.0mm 2.75mm 1.6mm 0.4mm 1.75mm "" "+" "square"]
)
