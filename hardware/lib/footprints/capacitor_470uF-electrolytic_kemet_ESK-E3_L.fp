# footprint for an aluminum electronlytic capacitor
# manufacturer: Kemet
# series: ESK
# size code: E3
# datasheet: http://www.kemet.com/docfinder?Partnumber=ESK477M6R3AE3AA
# class: least
Element["" "capacitor" "" "Kemet ESK E3 least" 0 0 0 0 0 100 ""]
(
ElementArc[0.0mm 0.0mm 3.15mm 3.15mm 0 360 0.2mm]
ElementLine[-4.15mm 0.0mm -3.65mm 0.0mm 0.2mm]
ElementLine[-3.9mm -0.25mm -3.9mm 0.25mm 0.2mm]
ElementLine[4.15mm 0.0mm 3.65mm 0.0mm 0.2mm]
Pin[-1.25mm 0.0mm 0.95mm 0.2mm 1.05mm 0.65mm "+" "+" "square"]
Pin[1.25mm 0.0mm 0.95mm 0.2mm 1.05mm 0.65mm "-" "-" ""]
)
