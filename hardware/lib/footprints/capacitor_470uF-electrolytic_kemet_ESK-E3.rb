#!/usr/bin/env ruby
# encoding: utf-8
# written for ruby 2.1.0
# generate a footprint (see script for more information)
require_relative 'element'

# global dimensions
UNIT = "mm"
SILKSCREEN = 0.2
SOLDERMASK = 0.1

# class properties, for radial leads and perpendicular parts
classes = []
classes << {:class => "least", :hole => 0.15, :ring => 0.3, :clearance => 0.5}
classes << {:class => "nominal", :hole => 0.2, :ring => 0.35, :clearance => 0.7}
classes << {:class => "maximum", :hole => 0.25, :ring => 0.5, :clearance => 1.0}

# size code dimensions
D = 6.3
p = 2.5
d = 0.5

classes.each do |cla|
  base = File.basename(__FILE__,".rb")
  name = "#{base}_#{cla[:class][0,1].upcase}.fp"
  File.open(name,"w") do |fp|
    # put some information
    fp.puts "# footprint for an aluminum electronlytic capacitor"
    fp.puts "# manufacturer: Kemet"
    fp.puts "# series: ESK"
    fp.puts "# size code: E3"
    fp.puts "# datasheet: http://www.kemet.com/docfinder?Partnumber=ESK477M6R3AE3AA"
    fp.puts "# class: #{cla[:class]}"

    # define element
    # center is pin 3
    fp.puts element("Element",["","capacitor","","Kemet ESK E3 #{cla[:class]}",:"0",:"0",:"0",:"0",:"0",:"100",""])
    fp.puts "("

    # outline
    #ElementArc [X Y Width Height StartAngle DeltaAngle Thickness]
    fp.puts element("ElementArc",[0,0,D/2,D/2,:"0",:"360",SILKSCREEN])
    #ElementLine [X1 Y1 X2 Y2 Thickness]
    fp.puts element("ElementLine",[-D/2-1,0,-D/2-0.5,0,SILKSCREEN])
    fp.puts element("ElementLine",[-D/2-0.75,-0.25,-D/2-0.75,0.25,SILKSCREEN])
    fp.puts element("ElementLine",[D/2+1,0,D/2+0.5,0,SILKSCREEN])

    # pins
    # Pin [rX rY Thickness Clearance Mask Drill "Name" "Number" SFlags]
    fp.puts element("Pin",[-p/2,0,d+cla[:hole]+cla[:ring],cla[:clearance]-cla[:ring],d+cla[:hole]+cla[:ring]+SOLDERMASK,d+cla[:hole],"+","+","square"])
    fp.puts element("Pin",[p/2,0,d+cla[:hole]+cla[:ring],cla[:clearance]-cla[:ring],d+cla[:hole]+cla[:ring]+SOLDERMASK,d+cla[:hole],"-","-",""])

    # end of element
    fp.puts ")"
  end
end
