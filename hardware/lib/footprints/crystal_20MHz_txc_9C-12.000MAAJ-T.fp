# footprint for a SMT crystal HC-49S
# manufacturer: TXC CORPORATION
# part number: 9C-12.000MAAJ-T
# datasheet: http://www.txccrystal.com/images/pdf/9c.pdf
Element["" "HC-49S" "" "TXC 9C-12.000MAAJ-T" 0 0 0 0 0 100 ""]
(
ElementLine[-6.35mm -2.4mm 6.35mm -2.4mm 0.2mm]
ElementLine[6.35mm -2.4mm 6.35mm 2.4mm 0.2mm]
ElementLine[6.35mm 2.4mm -6.35mm 2.4mm 0.2mm]
ElementLine[-6.35mm 2.4mm -6.35mm -2.4mm 0.2mm]
Pad[-3.0mm 0.0mm -6.5mm 0.0mm 2.0mm 0.4mm 2.15mm "" "1" "square"]
Pad[3.0mm 0.0mm 6.5mm 0.0mm 2.0mm 0.4mm 2.15mm "" "2" "square"]
)
