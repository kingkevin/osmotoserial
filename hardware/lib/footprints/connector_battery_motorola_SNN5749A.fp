# footprint as motorola SNN5749A battery
# manufacturer: Motorola
# part number: SNN5749A
Element["" "Motorola SNN5749A" "" "battery" 0 0 0 0 0 100 ""]
(
ElementLine[-18.0mm 28.0mm -15.0mm 28.0mm 0.2mm]
ElementLine[-15.0mm 28.0mm -15.0mm 29.0mm 0.2mm]
ElementLine[-15.0mm 29.0mm -12.0mm 29.0mm 0.2mm]
ElementLine[-12.0mm 29.0mm -12.0mm 28.0mm 0.2mm]
ElementLine[-12.0mm 28.0mm 12.0mm 28.0mm 0.2mm]
ElementLine[12.0mm 28.0mm 12.0mm 29.0mm 0.2mm]
ElementLine[12.0mm 29.0mm 15.0mm 29.0mm 0.2mm]
ElementLine[15.0mm 29.0mm 15.0mm 28.0mm 0.2mm]
ElementLine[15.0mm 28.0mm 18.0mm 28.0mm 0.2mm]
ElementLine[18.0mm 28.0mm 18.0mm -28.0mm 0.2mm]
ElementLine[18.0mm -28.0mm 4.7mm -28.0mm 0.2mm]
ElementLine[4.7mm -28.0mm 4.7mm -28.5mm 0.2mm]
ElementLine[4.7mm -28.5mm -2.15mm -28.5mm 0.2mm]
ElementLine[-2.15mm -28.5mm -2.15mm -28.0mm 0.2mm]
ElementLine[-2.15mm -28.0mm -18.0mm -28.0mm 0.2mm]
ElementLine[-18.0mm -28.0mm -18.0mm 28.0mm 0.2mm]
Pad[-14.9mm -27.2mm -14.9mm -24.3mm 1.6mm 0.4mm 1.75mm "" "-" "square"]
Pad[-12.4mm -27.2mm -12.4mm -24.3mm 1.6mm 0.4mm 1.75mm "" "" "square"]
Pad[-9.9mm -27.2mm -9.9mm -24.3mm 1.6mm 0.4mm 1.75mm "" "+" "square"]
)
